import React from 'react';
import ReactDOM from 'react-dom';
import { Col, ListGroup, ListGroupItem, Panel } from 'react-bootstrap';

const storeManager = require('../storeManager/storeManager.js');

require('./QuestionDetails.scss');

class QuestionDetails extends React.Component {
    constructor (props) {
        super(props);
        this.storeManager = storeManager.getQuestionsStore();
        
        this.storeManager.subscribe(()=>{
            var state = this.storeManager.getState();
            if (state.questionaire && state.questionaire.answersLoaded) {
                this.setState({
                    answerLoaded: true
                });
            }
        });
        this.state = {};
    }

    changeAnswer (evt) {
        var selectedValueIndex = parseInt(evt.target.value, 10);
        
        this.storeManager.dispatch({
            type: 'SET_ANSWER',
            question: this.props.questionIndex,
            answer: selectedValueIndex
        });
    }

    render () {
        return <Col className="question-details" xs={10}>
                <Panel header={this.props.question.text} bsStyle="warning">
                    <ListGroup className="question-details__options">
                        {this.getOptionsDom()}
                    </ListGroup>
                </Panel>
            </Col>;
    }

    getOptionsDom () {
        return this.props.question.options.map((option, optionIndex)=>{
            return this.getSingleOptionDom(option, optionIndex);
        });
    }

    getSingleOptionDom (option, optionIndex) {
        return this.state.answerLoaded ?
            this.getAnsweredOptionDom(option, optionIndex) :
            this.getRadioOptionDom(option, optionIndex);
    }

    getAnsweredOptionDom(option, optionIndex) {
        return <ListGroupItem key={optionIndex} bsStyle={this.getBsStyle(optionIndex)}>
            {option}
        </ListGroupItem>;
    }

    getRadioOptionDom(option, optionIndex) {
        return <ListGroupItem key={optionIndex}>
                <label>
                    <input type="radio" value={optionIndex} checked={this.props.question.answer === optionIndex} onChange={this.changeAnswer.bind(this)} />
                    {option}
                </label>
            </ListGroupItem>;
    }

    getBsStyle (optionIndex) {
        if (!this.state.answerLoaded || this.props.question.answer !== optionIndex) {
            return 'info';
        } else {
            return this.props.question.isCorrect ? 'success' : 'danger';
        }
    }
}

module.exports = QuestionDetails;
