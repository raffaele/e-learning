import React from 'react';
import ReactDOM from 'react-dom';
import { Col, ListGroup, ListGroupItem } from 'react-bootstrap';

const storeManager = require('../storeManager/storeManager.js');

require('./QuestionIndex.scss');

class QuestionIndex extends React.Component {
    constructor (props) {
        const questionStore = storeManager.getQuestionsStore();

        super(props);
        this.state = {};
        
        this.questionsStore = questionStore;
        questionStore.subscribe(()=>{
            var questionaireState = questionStore.getState().questionaire;
            if (questionaireState.questionaireCompleted) {
                this.updateResults(questionaireState);
            }
        });
    }

    updateResults (questionState) {
        this.setState({
            isComplete: true,
            totalQuestions: questionState.questions.length,
            correctAnswers: questionState.questions.filter((question)=>question.isCorrect).length
        });
    }

    selectQuestion (questionIndex) {
        this.questionsStore.dispatch({
            type: 'SELECT_QUESTION',
            selectedQuestion: questionIndex
        });
    }

    getResponseClass (questionIndex) {
        var question = this.props.questions[questionIndex];
        return (typeof question.answer !== 'undefined') ?
            'question-index__link--answered' :
            '';
    }

    getBsStyle (questionIndex) {
        var question = this.props.questions[questionIndex];

        if (!this.state.isComplete) {
            return 'info';
        } else {
            return question.isCorrect?'success':'danger';
        }
    }

    render () {
        return <Col className="question-index" xs={2}>
                    <ListGroup>
                        {this.props.questions.map((question, questionIndex)=>{
                            return <ListGroupItem bsStyle={this.getBsStyle(questionIndex)} className={'question-index__link ' + this.getResponseClass(questionIndex)} key={questionIndex} onClick={this.selectQuestion.bind(this, questionIndex)}>
                                    {questionIndex === this.props.questionIndex ?
                                        <strong>Question {1+questionIndex}</strong> :
                                        <span>Question {1+questionIndex}</span> }    
                                </ListGroupItem>;
                        })}
                        
                    </ListGroup>
                    {this.state.isComplete &&
                        <ListGroup>
                            <ListGroupItem className="question-index__final-result">
                                {this.state.correctAnswers}/{this.state.totalQuestions}
                            </ListGroupItem>
                        </ListGroup>
                    }
                    
            </Col>;
    }
}

module.exports = QuestionIndex;
