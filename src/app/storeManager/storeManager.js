const api = require('../api/api.js'),
    redux = require('redux'),
    $ = require('jquery');

const initialState = {
    questionaire: {
        questions: [],
        questionsLoaded: false,
        answersLoaded: false
    },
    selectedQuestionIndex: 0
};

const reducers = redux.combineReducers({
    questionaire: questionReducer,
    selectedQuestionIndex: questionSelectReducer
});

function questionSelectReducer (state = initialState.selectedQuestionIndex, action) {
    switch (action.type) {
        case 'SELECT_QUESTION':
            return action.selectedQuestion
    }

    return state;
}

function questionReducer (state = initialState.questionaire, action) {
    let newState = $.extend({}, state);
    switch (action.type) {
        case 'RECEIVE_QUESTIONS':
            return $.extend(newState, {questions: action.questions, questionsLoaded: true, questionsLoadedError: false});
        case 'RECEIVE_QUESTIONS_ERROR':
            return $.extend(newState, {questions: [], questionsLoaded: false, questionsLoadedError: true});
        case 'SET_ANSWER':
            newState.questions[action.question].answer = action.answer;
            return newState;
        case 'STORE_RESULTS':
            newState.questionaireCompleted = true;
            newState.questions.forEach((question, questionIndex)=>{
                question.isCorrect = action.correctAnswers[questionIndex];
            });
            newState.answersLoaded = true;
            return newState;
    }

    return newState;
}



var store;

function requireQuestions () {
    api.getQuestions().then((questionResponse)=>{
        store.dispatch({
            type: 'RECEIVE_QUESTIONS',
            questions: questionResponse.questions
        });
    }, (err) => {
        store.dispatch({
            type: 'RECEIVE_QUESTIONS_ERROR',
            questions: [],
            errorDetails: err
        });
    });
}

function getQuestionsStore () {
    if (!store) {
        store = redux.createStore(reducers);
        requireQuestions();
    }

    return store;
}

module.exports = {
    getQuestionsStore: getQuestionsStore,
    requireQuestions: requireQuestions
};
