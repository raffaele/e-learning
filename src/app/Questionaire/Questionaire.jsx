import React from 'react';
import ReactDOM from 'react-dom';

const QuestionIndex = require('../QuestionIndex/QuestionIndex.jsx'),
    QuestionDetails = require('../QuestionDetails/QuestionDetails.jsx'),
    storeManager = require('../storeManager/storeManager.js');

class Questionaire extends React.Component {
    constructor (props) {
        super(props);
        
        this.questionsStore = storeManager.getQuestionsStore();
        var actualState = this.questionsStore.getState();
        this.state = {
            selectedQuestion: actualState.questionaire.questions[0],
            selectedQuestionIndex: 0
        };
        this.questionsStore.subscribe(()=>{
            var actualState = this.questionsStore.getState();
            this.setState({
                selectedQuestion: actualState.questionaire.questions[actualState.selectedQuestionIndex],
                selectedQuestionIndex: parseInt(actualState.selectedQuestionIndex, 10)
            });
        });
    }

    render () {
        return <div className="questionaire">
                <QuestionIndex questions={this.props.questions} questionIndex={this.state.selectedQuestionIndex} />
                <QuestionDetails question={this.state.selectedQuestion} questionIndex={this.state.selectedQuestionIndex} />
            </div>;
    }
}

module.exports = Questionaire;
