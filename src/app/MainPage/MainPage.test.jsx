const { expect } = require('chai');
const { shallow } = require('enzyme');
const React = require('react');
const sinon = require('sinon');

const MainPage = require('./MainPage.jsx'),
    Questionaire = require('../Questionaire/Questionaire.jsx');


describe('<MainPage />', () => {
    it('should contain the proper doms before receiving the questions', () => {
        var wrapper = shallow(<MainPage />);
        expect(wrapper.find('.main-page-loader').length).to.be.equal(1);
        expect(wrapper.find('.main-page-submit-area').length).to.be.equal(0);
        expect(wrapper.find(Questionaire).length).to.be.equal(0);
    });

    it('should contain the proper doms after receiving the questions', () => {
        var wrapper = shallow(<MainPage />);
        var questions = [];
        wrapper.setState({
            questionsLoaded: true,
            questions: questions
        });
        expect(wrapper.find('.main-page-loader').length).to.be.equal(0);
        expect(wrapper.find('.main-page-submit-area').length).to.be.equal(0);
        expect(wrapper.find(Questionaire).length).to.be.equal(1);
        expect(wrapper.find(Questionaire).props().questions).to.be.equal(questions);
    });

});