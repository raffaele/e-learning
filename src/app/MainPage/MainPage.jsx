import React from 'react';
import ReactDOM from 'react-dom';
import { Col, Button, PageHeader, Label } from 'react-bootstrap';

const storeManager = require('../storeManager/storeManager.js'),
    Questionaire = require('../Questionaire/Questionaire.jsx'),
    api = require('../api/api.js');

class MainPage extends React.Component {
    constructor (props) {
        super(props);
        this.state = {};

        var store = storeManager.getQuestionsStore();
        this.store = store;

        store.subscribe(()=>{
            const actualState = store.getState().questionaire;
            if (actualState.questionsLoaded || actualState.questionsLoadedError) {
                this.setState({
                    questionsLoaded: actualState.questionsLoaded,
                    questions: actualState.questions,
                    questionaireCompleted: actualState.questionaireCompleted,
                    answersLoaded: actualState.answersLoaded,
                    questionsLoadedError: actualState.questionsLoadedError
                });
            }
        });

    }
    isSubmitPossible () {
        return !this.state.answersLoaded &&
            this.state.questions &&
            this.state.questions.every((question) => {
                return question.answer !== undefined;
            });
    }

    submitAnswers () {
        var answers = this.state.questions.map(question=>question.answer);
        api.sendResponses(answers).then((response)=>{
            this.store.dispatch({
                type: 'STORE_RESULTS',
                correctAnswers: response.answers
            });
        }, (err) => {
            alert('We found an issue on comunication with the server, please check your connection and try again');
        });
    }

    render () {
        return <div className="main-page container">
                {this.state.questionsLoadedError ? this.getDomWithApiError() : this.getDomWithoutApiError()}
            </div>;
    }

    getDomWithoutApiError () {
        return <div>
            <Col>
                <PageHeader>Questionaire <small>Do your best</small></PageHeader>
            </Col>
            {this.state.questionsLoaded ?
                <Questionaire questions={this.state.questions} /> : 
                <Col className="main-page-loader" bsClass="container">LOADING...</Col>}
            {this.isSubmitPossible() && <div className="main-page-submit-area container">
                    <Button onClick={this.submitAnswers.bind(this)} bsStyle="primary">Submit your answers</Button>
            </div>}
        </div>;
    }

    sendQuestionRequest () {
        storeManager.requireQuestions();
    }

    getDomWithApiError () {
        return <div>
                <h1><Label bsStyle="danger">Api error</Label></h1>
                <Button bsStyle="primary" onClick={this.sendQuestionRequest}>Please try again</Button>
            </div>;
    }
}

module.exports = MainPage;
