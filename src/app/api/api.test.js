const { expect } = require('chai'),
    api = require('./api.js'),
    jquery = require('jquery'),
    sinon = require('sinon');

describe('api', () => {
    describe('getQuestions method', () => {
        it('should call the jquery.get method and return the result of the call', () => {
            var apiResponse;

            sinon.spy(jquery, 'get');
            apiResponse = api.getQuestions();
            expect(jquery.get.calledWith('http://localhost:3000/questions')).to.be.equal(true);
        });
    });

    describe('sendResponses method', () => {
        it('should call the jquery.post method and return the result of the call', () => {
            var apiResponse,
                responsesToSend = [1, 2, 5, 6];

            sinon.spy(jquery, 'post');
            apiResponse = api.sendResponses(responsesToSend);
            expect(jquery.post.calledWith('http://localhost:3000/answers', {responses: responsesToSend})).to.be.equal(true);
        });
    });
});
