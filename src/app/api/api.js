const config = require('../../config.json'),
    jquery = require('jquery');

module.exports = {
    getQuestions: getQuestions,
    sendResponses: sendResponses
};

function sendResponses (responses) {
    return jquery.post(`${config.apiBaseUrl}/answers`, {responses: responses});
}

function getQuestions () {
    return jquery.get(`${config.apiBaseUrl}/questions`);
}
