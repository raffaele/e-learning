import React from 'react';
import ReactDOM from 'react-dom';

const MainPage = require('./MainPage/MainPage.jsx');

class App extends React.Component {
    constructor (props) {
        super(props);
    }
    render () {
        return <div className="app">
                <MainPage></MainPage>
            </div>;
    }
}

ReactDOM.render(
    <App />,
    document.getElementById('main-app')
);
