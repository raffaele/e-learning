E-LEARNING
======

Dependencies: [Node](https://nodejs.org/) (version 6.5.0). All other dependencies are defined in `package.json` file.

Instructions to run the project:

1. Clone the repository from git
2. Run the command `npm install` to download all the dependencies
3. Run the command `npm run api` to launch the server. The server should keep working for all the test, it can be stopped just to simulate the network issues.
4. Run the command `npm run dev` to launch the app (the browser will be automatically opened)
