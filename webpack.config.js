const HtmlWebpackPlugin = require('html-webpack-plugin'),
    path = require('path'),
    ExtractTextPlugin = require('extract-text-webpack-plugin');

const isProd = false;

module.exports = {
    entry: {
        app: './src/app/app.jsx'
    },
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: '[name].js'
    },
    module: {
        rules: [{
            test: /\.scss$/,
            use: ExtractTextPlugin.extract({
                fallbackLoader: 'style-loader',
                loader: ['css-loader','sass-loader'],
                publicPath: 'dist'
            })
        } , {
            test: /\.jsx?$/,
            exclude: /node_modules/,
            use: 'babel-loader'
        }]
    },
    devServer: {
        contentBase: path.join(__dirname, 'dist'),
        stats: 'errors-only',
        compress: true,
        open: true,
        port: 8081,
        proxy: {
            '/stubs/*': 'http://locahost:8081/api',
            secure: false
        }
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: './src/index.html',
            minify: {
                collapseWhitespace: false
            },
            hash: true
        }),
        new ExtractTextPlugin({
            filename: 'app.css',
            disable: false,
            allChunks: true
        })
    ]
};
