var express = require('express');
var app = express();

const SERVER_PORT = parseInt(process.argv[2], 10) || 3000;
const NUMBER_OF_QUESTIONS = 5;
const options = Array(4).fill().map((useless, optionIndex)=>{
    return `Answer number ${1+optionIndex}`;
});

const questions = Array(NUMBER_OF_QUESTIONS).fill().map((useless, questionIndex)=> {
    return {
        text: `My question number ${1+questionIndex}`,
        options: [...options]
    };
});

app.get('/questions', (req, res) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.json({questions: questions});
});

app.post('/answers', (req, res) => {
    var answers = Array(NUMBER_OF_QUESTIONS).fill().map(()=>{
        return Math.random() > .2;
    });
    res.header('Access-Control-Allow-Origin', '*');
    res.json({answers: answers});
});

app.listen(SERVER_PORT, function () {
  console.log(`Example app listening on port ${SERVER_PORT}!`)
});
